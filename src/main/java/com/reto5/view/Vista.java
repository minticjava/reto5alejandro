package com.reto5.view;


import java.sql.SQLException;
import java.util.ArrayList;

import com.reto5.controller.Controlador;
import com.reto5.model.vo.Lider;
import com.reto5.model.vo.Proyecto;

public class Vista {

    public static final Controlador controlador = new Controlador();

    public static void vista_requerimiento_1() {

        try {
            String resultado = "";
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                resultado = resultado 
                + "Constructora: "+proyectos.get(i).getNombre_constructora()
                + " - Serial: "+proyectos.get(i).getSerial()+"\n";
           }
           System.out.println(resultado);
        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_2() {
        try {
            String resultado = "";
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            for(int i = 0; i< proyectos.size(); i++){
                resultado = resultado + "Numero_Habitaciones: "+proyectos.get(i).getNum_habitaciones()
                +" - Numero_Banos: "+proyectos.get(i).getNum_banios()
                +" - Nombre_Lider: "+proyectos.get(i).getLider().getNombre()
                +" - Apellido_Lider: "+proyectos.get(i).getLider().getApellido()
                +" - Estrato_Proyecto: "+proyectos.get(i).getEstrato_proyecto()+"\n";
            }
            System.out.println(resultado);
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_3() {
        try {
            String resultado = "";
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_3();
            for(int i = 0; i< proyectos.size(); i++){
                resultado = resultado + "Cantidad_Casas: "+proyectos.get(i).getNum_casas()
                + " - Constructora: " + proyectos.get(i).getNombre_constructora() + "\n";
                
            }
            System.out.println(resultado);
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_4() {
        try {
            String resultado = "";
            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for(int i = 0; i<lideres.size();i++){
                resultado = resultado + "Nombre_Lider: " + lideres.get(i).getNombre()
                + " - Apellido_Lider: " + lideres.get(i).getApellido()+"\n";
            }
            System.out.println(resultado);
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_5() {
        try {
            String resultado = "";
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_5();
            for(int i = 0; i<proyectos.size();i++){
                resultado = resultado + "Cantidad_Casas: " + proyectos.get(i).getNum_casas()
                + " - Constructora: " + proyectos.get(i).getNombre_constructora() + "\n";
            }
            System.out.println(resultado);
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

}
