package com.reto5.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.reto5.controller.Controlador;
import com.reto5.model.vo.Lider;
import com.reto5.model.vo.Proyecto;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class Formulario extends JFrame{

    public static final Controlador controlador = new Controlador();

    private JLabel lblTitulo;
    private JButton btnRequerimiento1;
    private JButton btnRequerimiento2;
    private JButton btnRequerimiento3;
    private JButton btnRequerimiento4;
    private JButton btnRequerimiento5;

    private Container centerContainer;
    
    public Formulario(){

        this.setTitle("Reto 5");
        this.setBounds(0,0,900,300);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        GridLayout northGritLayout = new GridLayout(1,3);
        Container northContainer = new Container();
        northContainer.setLayout(northGritLayout);

        northContainer.add(new JLabel());
        this.lblTitulo = new JLabel("Bienvenido al reto 5 ");
        northContainer.add(this.lblTitulo);
        northContainer.add(new JLabel());
        this.add(northContainer,BorderLayout.NORTH);

        GridLayout westGritLayout = new GridLayout(5,1);
        Container westContainer = new Container();
        westContainer.setLayout(westGritLayout);

        GridLayout centerGridLayout = new GridLayout(1,1);
        centerContainer = new Container();
        centerContainer.setLayout(centerGridLayout);

        this.btnRequerimiento1 = new JButton("Requerimiento 1");
        westContainer.add(this.btnRequerimiento1);


        this.btnRequerimiento1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                requerimiento1();
            }
        });

        this.btnRequerimiento2 = new JButton("Requerimiento 2");
        westContainer.add(this.btnRequerimiento2);

        this.btnRequerimiento2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                requerimiento2();
            }
        });

        this.btnRequerimiento3 = new JButton("Requerimiento 3");
        westContainer.add(this.btnRequerimiento3);

        this.btnRequerimiento3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                requerimiento3();
            }
        });

        this.btnRequerimiento4 = new JButton("Requerimiento 4");
        westContainer.add(this.btnRequerimiento4);

        this.btnRequerimiento4.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                requerimiento4();
            }
        });

        this.btnRequerimiento5 = new JButton("Requerimiento 5");
        westContainer.add(this.btnRequerimiento5);

        this.btnRequerimiento5.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                requerimiento5();
            }
        });

        this.add(westContainer,BorderLayout.WEST);

    }

    private void requerimiento1(){
        centerContainer.removeAll();
        String[] titulo = {"Constructora","Serial"};
        String[][] datos={};
        DefaultTableModel dtm = new DefaultTableModel(datos,titulo);
        final JTable tabla1 = new JTable(dtm);
        try {
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                Object[] newRow = {proyectos.get(i).getNombre_constructora(), proyectos.get(i).getSerial()};
                dtm.addRow(newRow);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

        tabla1.setPreferredScrollableViewportSize(new Dimension(350, 200));
        JScrollPane scrollPane = new JScrollPane(tabla1);
        centerContainer.add(scrollPane);
        add(centerContainer, BorderLayout.CENTER);
    }

    private void requerimiento2(){
        centerContainer.removeAll();
        String[] titulo = {"Numero_Habitaciones","Numero_Banos","Nombre_Lider","Apellido_Lider","Estrato_Proyecto"};
        String[][] datos={};
        DefaultTableModel dtm = new DefaultTableModel(datos,titulo);
        final JTable tabla1 = new JTable(dtm);
        try {
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_2();
            for (int i = 0; i < proyectos.size(); i++) {
                Object[] newRow = {proyectos.get(i).getNum_habitaciones(), proyectos.get(i).getNum_banios(),proyectos.get(i).getLider().getNombre(),proyectos.get(i).getLider().getApellido(),proyectos.get(i).getEstrato_proyecto()};
                dtm.addRow(newRow);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

        tabla1.setPreferredScrollableViewportSize(new Dimension(350, 200));
        JScrollPane scrollPane = new JScrollPane(tabla1);
        centerContainer.add(scrollPane);
        add(centerContainer, BorderLayout.CENTER);
    }

    private void requerimiento3(){
        centerContainer.removeAll();
        String[] titulo = {"Cantidad_Casas","Constructora"};
        String[][] datos={};
        DefaultTableModel dtm = new DefaultTableModel(datos,titulo);
        final JTable tabla1 = new JTable(dtm);
        try {
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_3();
            for (int i = 0; i < proyectos.size(); i++) {
                Object[] newRow = {proyectos.get(i).getNum_casas(), proyectos.get(i).getNombre_constructora()};
                dtm.addRow(newRow);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

        tabla1.setPreferredScrollableViewportSize(new Dimension(350, 200));
        JScrollPane scrollPane = new JScrollPane(tabla1);
        centerContainer.add(scrollPane);
        add(centerContainer, BorderLayout.CENTER);
    }

    private void requerimiento4(){
        centerContainer.removeAll();
        String[] titulo = {"Nombre_Lider","Apellido_Lider"};
        String[][] datos={};
        DefaultTableModel dtm = new DefaultTableModel(datos,titulo);
        final JTable tabla1 = new JTable(dtm);
        try {
            ArrayList<Lider> lideres = controlador.Solucionar_requerimiento_4();
            for (int i = 0; i < lideres.size(); i++) {
                Object[] newRow = {lideres.get(i).getNombre(), lideres.get(i).getApellido()};
                dtm.addRow(newRow);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

        tabla1.setPreferredScrollableViewportSize(new Dimension(350, 200));
        JScrollPane scrollPane = new JScrollPane(tabla1);
        centerContainer.add(scrollPane);
        add(centerContainer, BorderLayout.CENTER);
    }

    private void requerimiento5(){
        centerContainer.removeAll();
        String[] titulo = {"Cantidad_Casas","Constructora"};
        String[][] datos={};
        DefaultTableModel dtm = new DefaultTableModel(datos,titulo);
        final JTable tabla1 = new JTable(dtm);
        try {
            ArrayList<Proyecto> proyectos = controlador.Solucionar_requerimiento_5();
            for (int i = 0; i < proyectos.size(); i++) {
                Object[] newRow = {proyectos.get(i).getNum_casas(), proyectos.get(i).getNombre_constructora()};
                dtm.addRow(newRow);
            }
        } catch (Exception e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
        

        tabla1.setPreferredScrollableViewportSize(new Dimension(350, 200));
        JScrollPane scrollPane = new JScrollPane(tabla1);
        centerContainer.add(scrollPane);
        add(centerContainer, BorderLayout.CENTER);
    }
}
