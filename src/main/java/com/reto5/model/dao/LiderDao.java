package com.reto5.model.dao;

//Estructura de datos
import java.util.ArrayList;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import com.reto5.util.JDBCUtilities;

//Encapsulamiento de los datos
import com.reto5.model.vo.Lider;

public class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();
        // Consultas
        
        String query = "select l.Nombre, l.Primer_Apellido from Proyecto p inner join Lider l on p.ID_Lider = l.ID_Lider where p.Clasificacion = \"Casa\"";

        PreparedStatement pStatement = conexion.prepareStatement(query);
        ResultSet resultado = pStatement.executeQuery();

        while(resultado.next()){
            Lider objLider = new Lider(resultado.getString("nombre"),resultado.getString("Primer_Apellido"));
            lideres.add(objLider);
        }

        return lideres;
    }// Fin del método query_requerimiento_4

}