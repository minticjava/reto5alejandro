package com.reto5.model.dao;

//Estructura de datos
import java.util.ArrayList;

import com.reto5.model.vo.Lider;
import com.reto5.model.vo.Proyecto;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

//Clase para conexión
import com.reto5.util.JDBCUtilities;

public class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();
        
        String query = "select Constructora, Serial from Proyecto where Clasificacion = \"Casa\"";
        PreparedStatement pStatement= conexion.prepareStatement(query);
        ResultSet resultado = pStatement.executeQuery();

        while(resultado.next()){
            Proyecto objProyecto = new Proyecto();
            objProyecto.setNombre_constructora(resultado.getString("Constructora"));
            objProyecto.setSerial(resultado.getString("Serial"));
            proyectos.add(objProyecto);
        }

        return proyectos;
    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        String query = "select p.Numero_Habitaciones, p.Numero_Banos, l.Nombre, l.Primer_Apellido, t.Estrato from Proyecto p inner join Lider l on p.ID_Lider  = l.ID_Lider inner join Tipo t on p.ID_Tipo = t.ID_Tipo where p.Clasificacion = \"Casa\" limit 50";

        PreparedStatement pStatement = conexion.prepareStatement(query);
        ResultSet resultado = pStatement.executeQuery();

        while(resultado.next()){
            Proyecto objProyecto = new Proyecto();
            Lider objLider = new Lider(resultado.getString("Nombre"),resultado.getString("Primer_Apellido"));
            objProyecto.setNum_habitaciones(resultado.getInt("Numero_Habitaciones"));
            objProyecto.setNum_banios(resultado.getInt("Numero_Banos"));
            objProyecto.setLider(objLider);
            objProyecto.setEstrato_proyecto(resultado.getInt("Estrato"));
            proyectos.add(objProyecto);
        }
        return proyectos;
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        Connection conexion = JDBCUtilities.getConnection();
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        String query = "select COUNT() as \"Casas\", Constructora from Proyecto where Clasificacion = \"Casa\" group by Constructora ";

        PreparedStatement pStatement = conexion.prepareStatement(query);
        ResultSet resultado = pStatement.executeQuery();

        while(resultado.next()){
            Proyecto objProyecto = new Proyecto();
            objProyecto.setNum_casas(resultado.getInt("Casas"));
            objProyecto.setNombre_constructora(resultado.getString("Constructora"));
            proyectos.add(objProyecto);
        }

        return proyectos;
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        Connection conexion = JDBCUtilities.getConnection();
        ArrayList<Proyecto> proyectos = new ArrayList<Proyecto>();

        String query = "select count(p.Clasificacion) as Casas, Constructora from Proyecto p where p.Clasificacion = \"Casa\" GROUP BY Constructora HAVING Casas >= 18 ORDER BY Casas ASC;";

        PreparedStatement pStatement = conexion.prepareStatement(query);
        ResultSet resultado = pStatement.executeQuery();

        while(resultado.next()){
            Proyecto objProyecto = new Proyecto();
            objProyecto.setNum_casas(resultado.getInt("Casas"));
            objProyecto.setNombre_constructora(resultado.getString("Constructora"));
            proyectos.add(objProyecto);
        }
        return proyectos;
    }// Fin del método query_requerimiento_4

}