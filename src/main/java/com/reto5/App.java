package com.reto5;
import com.reto5.view.Formulario;
import com.reto5.view.Vista;

/**
 * Persistencia Proyectos Construcción
 *
 */
public class App {
    public static void main(String[] args) {

        // Casos de prueba

        // Requerimiento 1 - Reto3
        //Vista.vista_requerimiento_1();
        //Vista.vista_requerimiento_2();
        //Vista.vista_requerimiento_3();
        //Vista.vista_requerimiento_4();
        //Vista.vista_requerimiento_5();
        Formulario objFormulario = new Formulario();
        objFormulario.setVisible(true);

    }
}
